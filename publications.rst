Publications
---------------------

* An acknowledgment of Federal support and a disclaimer must appear in the publication of any material, whether copyrighted or not, based on or developed under this project, as follows:

Acknowledgment: 
"This material is based upon work supported by the Department of Energy, National Nuclear Security Administration under Award Number DE-NA0003962."

Disclaimer: "This report was prepared as an account of work sponsored by an agency of the United States Government. Neither the United States Government nor any agency thereof, nor any of their employees, makes any warranty, express or implied, or assumes any legal liability or responsibility for the accuracy, completeness, or usefulness of any information, apparatus, product, or process disclosed, or represents that its use would not infringe privately owned rights. Reference herein to any specific commercial product, process, or service by trade name, trademark, manufacturer, or otherwise does not necessarily constitute or imply its endorsement, recommendation, or favoring by the United States Government or any agency thereof. The views and opinions of authors expressed herein do not necessarily state or reflect those of the United States Government or any agency thereof."

* Recipients of DOE awards are required to submit all accepted peer-reviewed manuscripts produced with complete or partial DOE funding, to the Office of Scientific and Technical Information. Submissions are made to the DOE `E-Link <https://www.osti.gov/elink/>`_ system. Manuscripts must be submitted no later than one year after the journal publication date and before award closeout.

